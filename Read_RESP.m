%Simple script to read EFCC fields in h5 format
%L. Pigatto

fileU = 'B_RESP_3D_ITERlike';
Bphi = h5read(fileU,'/Bphi');
Br = h5read(fileU, '/Br');
Bz = h5read(fileU,'/Bz');
    
phi = h5read(fileU,'/phi');
R = h5read(fileU, '/R');
Z = h5read(fileU,'/Z');
    
%% gif of bpol
% set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex');
% M. Vallar
% for ss=1:10
%     Bpol = sqrt(squeeze(Br.r(ss, :,:)).^2+squeeze(Bz.r(ss, :,:)).^2);
%     figure(ss); contour(R,Z,Bpol');
% end
ds=10;

h = figure('Position', [1,1,1000,1500]);
%axis tight manual equal% this ensures that getframe() returns a consistent size
axis equal
filename = sprintf('Bpol_step_%i.gif', ds);
for ss = 1:ds:540
  % Draw plot for y = x.^n
  Bpol = squeeze(sqrt(sqrt(Br.r(ss, :,:).^2+Br.i(ss, :,:).^2).^2+...
      sqrt(Bz.r(ss, :,:).^2+Bz.r(ss, :,:).^2).^2));
  contourf(R,Z,Bpol');
  caxis([0, 0.013]);
  colormap('jet');
  c = colorbar;
  c.Label.String = 'B_{pol} T';
  title(sprintf('\\phi=%.2f \\pi', phi(ss)/(pi)));
  drawnow 
  % Capture the plot as an image 
  frame = getframe(h); 
  im = frame2im(frame); 
  [imind,cm] = rgb2ind(im,256); 
  % Write to the GIF File 
  if ss == 1 
      imwrite(imind,cm,filename,'gif', 'Loopcount',inf); 
  else 
      imwrite(imind,cm,filename,'gif','WriteMode','append'); 
  end 
end
close all;

%% bphi gif
h = figure('Position', [1,1,1000,1500]);
axis tight manual % this ensures that getframe() returns a consistent size
filename = sprintf('Bphi_step_%i.gif', ds);
for ss = 1:ds:540
  % Draw plot for y = x.^n
  Bphi_slice = squeeze(sqrt(Bphi.r(ss, :,:).^2+Bphi.i(ss, :,:).^2));
  contourf(R,Z,Bphi_slice');
  caxis([0 5e-3])
  colormap('jet');
  c = colorbar;
  c.Label.String = 'B_{phi} T';
  title(sprintf('\\phi=%.2f \\pi', phi(ss)/(pi)));
  drawnow 
  % Capture the plot as an image 
  frame = getframe(h); 
  im = frame2im(frame); 
  [imind,cm] = rgb2ind(im,256); 
  % Write to the GIF File 
  if ss == 1 
      imwrite(imind,cm,filename,'gif', 'Loopcount',inf); 
  else 
      imwrite(imind,cm,filename,'gif','WriteMode','append'); 
  end 
end
close all;